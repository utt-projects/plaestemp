from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=16, null=False, blank=False)
    age = models.IntegerField(default=0)
    weight = models.FloatField(default=1.0)
    status = models.BooleanField(default=True)
    timestamp = models.DateField(auto_now_add=True)
    date_update = models.DateField(auto_now=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.name


class Category(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=16)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    prodcut_name = models.CharField(max_length=16)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.prodcut_name