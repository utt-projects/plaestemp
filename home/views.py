from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from .models import Category
from .forms import UpdateCategoryForm, NewCategoryForm

# Create your views here.

# def index(request):
#     return render(request, "home/index.html", {})

class Index(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request):
        self.context = {
            "categories": Category.objects.all()
        }
        return render(request, self.template_name, self.context)



class DetailCategory(generic.View):
    template_name = "home/detail_category.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "category": Category.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)



class UpdateCategory(generic.UpdateView):
    template_name = "home/update_category.html"
    model = Category
    form_class = UpdateCategoryForm
    success_url = reverse_lazy("home:index")


class DeleteCategory(generic.DeleteView):
    template_name = "home/delete_category.html"
    model = Category
    success_url = reverse_lazy("home:index")


class NewCategory(generic.CreateView):
    template_name = "home/new_category.html"
    model = Category
    form_class = NewCategoryForm
    success_url = reverse_lazy("home:index")