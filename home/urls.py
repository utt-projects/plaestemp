from django.urls import path

from home import views

app_name = "home"

urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('new/category/', views.NewCategory.as_view(), name="new_category"),
    path('delete/category/<int:pk>/', views.DeleteCategory.as_view(), name="delete_category"),
    path('update/category/<int:pk>/', views.UpdateCategory.as_view(), name="update_category"),
    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name="detail_category"),
]